# Tutorial de programación de ARM Cortex-M con herramientas libres

Para este tutorial estoy utilizando:

* **HARDWARE:** Blue Pill (STM32F103C8) y ST-Link/V2
* **SOFTWARE:** PlatformIO en VS Code con libOpenCM3 sobre Debian GNU/Linux.

## Ejemplo 04: EXTIs

Este ejemplo está apuntado a la utilización básica de las interrupcines externas (EXTIs).

Entrada del blog: [Interrupciones externas con el STM32F1](https://electronlinux.wordpress.com/2020/06/18/interrupciones-externas-con-el-stm32f1/)

Video en YouTube: <>
