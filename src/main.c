/**
 * @file main.c
 * @author Matias S. Ávalos (Telegram: @tute_avalos)
 * @brief Ejemplo de interrupciones externas con libOpenCM3
 * @version 0.1
 * @date 2020-06-18
 * 
 * @copyright Copyright (c) 2020
 * 
 * MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>

#define FALLING false // Flanco descendente
#define RISING true   // Flanco ascendete

volatile uint32_t millis;      // cantidad de milisegundos
volatile bool trig;            // sentido del trigger
volatile uint32_t pulse_width; // cantidad de ms que dura el pulso

/**
 * @brief Demora bloqueante en milisegundos
 * 
 * @param ms cantidad de milisegundos a demorar.
 */
void delay_ms(uint32_t ms);

int main(void)
{
    // Configuración del SYSCLK
    rcc_clock_setup_in_hse_8mhz_out_72mhz();

    // PC13 (LED de la BluePill) como salida
    rcc_periph_clock_enable(RCC_GPIOC);
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
    gpio_set(GPIOC, GPIO13);

    // LED en PB6 (activo en alto), el PB15 como entrada flotante (default)
    rcc_periph_clock_enable(RCC_GPIOB);
    gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO6);
    gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO15);

    /* Para configurar las EXTI es necesario activar el 
       Alternative Functions Input/Output              */
    rcc_periph_clock_enable(RCC_AFIO);

    /* Se configura la interrupción EXTI15 en el GPIOB con flanco
       inicialmente descendente.                                 */
    exti_select_source(EXTI15, GPIOB);
    trig = FALLING;
    exti_set_trigger(EXTI15, EXTI_TRIGGER_FALLING);
    exti_enable_request(EXTI15);

    // Habilitación de la interrupción en el NVIC:
    nvic_enable_irq(NVIC_EXTI15_10_IRQ);

    // Se configura el SysTick para interrumpir cada 1ms:
    systick_set_frequency(1000, rcc_ahb_frequency);
    systick_counter_enable();
    systick_interrupt_enable();

    while (true)
    {
        if (pulse_width) // Si el pulso es distinto de 0:
        {
            uint32_t s = pulse_width / 1000; // Cantidad de segundos que duro el pulso
            s *= 2;                          // Doble para prender y apagar
            while (s--)                      // El LED PB6 titila la cantidad de
            {                                //    segundos que duró el pulso:
                gpio_toggle(GPIOB, GPIO6);
                delay_ms(300);
            }
            pulse_width = 0; // se resetea para que no se repita el "titileo"
        }
    }
}

void delay_ms(uint32_t ms)
{
    uint32_t tm = millis + ms;
    while (millis < tm)
        ;
}

void exti15_10_isr(void)
{
    static uint32_t lm = 0;
    exti_reset_request(EXTI15); // Se baja el flag
    if (trig == FALLING)        // Si fue un flanco descendente
    {
        lm = millis;               // Tomo la referencia de tiempo
        gpio_clear(GPIOC, GPIO13); // prende el LED de la BluePill
        trig = RISING;             // Se configura la interrupción por flanco ascendente
        exti_set_trigger(EXTI15, EXTI_TRIGGER_RISING);
    }
    else
    {
        pulse_width = millis - lm; // Se carga la longitud del pulso
        gpio_set(GPIOC, GPIO13);   // Se apaga el LED de la BluePill
        trig = FALLING;            // Se configura la interrupción por flanco descendente
        exti_set_trigger(EXTI15, EXTI_TRIGGER_FALLING);
    }
}

void sys_tick_handler(void)
{ // cada 1ms:
    millis++;
}